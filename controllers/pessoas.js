const pessoasModels =  require('../models/pessoas')

const index = async (connection, req, res) => {
    const params = {
        pageSize: req.query.pageSize || 10,
        currentPage: req.query.page || 0

    }
    const results = await pessoasModels.findAll(connection, params)
    res.render('pessoas/index', { results })
}

const deleteRoute = async (connection, req, res) => {
    await pessoasModels.deletePessoa(connection, req.params.id)
    res.redirect('/pessoas')
}

const createForm = (req, res) => {
    res.render('pessoas/create')
}
const createProcess =  async (connection, req, res) => {
    await pessoasModels.createPessoas(connection, req.body)
    //res.send(req.body)
    res.redirect('/pessoas')
}

const updateForm = async (connection, req, res) => {
    const pessoa = await pessoasModels.findbyID(connection, req.params.id)
    res.render('pessoas/update', { pessoa })
    //res.sender(pessoa)

}
const updateProcess =  async (connection, req, res) => {
    await pessoasModels.update(connection, req.params.id, req.body)
    //res.send(req.body)
    res.redirect('/pessoas')
}





module.exports = {
    index, deleteRoute, createForm, createProcess,updateForm,updateProcess
}