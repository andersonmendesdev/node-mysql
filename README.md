# CRUD
CRUD nodejs

Projeto simples de um CRUD (create, read, update e delete) em um banco de dados MYSQL.

Instalar as dependencias
```bash
npm install
```

Inicilizar servidor
```bash
npm start
```
Banco de dados utilizando foi o MySQL server 5.6

Database name
```bash
Dev
```
table name
```bash
cliente
```
attribute table
```bash
codcliente int(6)
nomecliente varchar(30)
endereco varchar(30)
cidade varchar(30)
cep varchar(11)
uf char(2)
cgc char(15)
ie varchar(15)
```