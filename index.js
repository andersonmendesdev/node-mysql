const express = require('express')
const path = require('path')
const app = express()
const port = process.env.PORT || 3000
const mysql = require('mysql')
const bodyParser = require('body-parser')

const connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '9240',
    database: 'dev'
})

//body parser
app.use(bodyParser.urlencoded({ extended: false }))

//object connection database
const dependencies = {
    connection
}

//directory public
app.use(express.static('public'))

//router
const pessoasRouter = require('./routes/pessoas')
app.get('/', (req, res) => res.render('home'))
app.use('/pessoas', pessoasRouter(dependencies))


//views
app.set('views', path.join(__dirname,'views'))
app.set('view engine', 'ejs')

//connection database and listening port
connection.connect(() => {
    app.listen(port, () => console.log('CRUD listening port '+port))
})
