const findAll = (connection, params) => {
    return new Promise ((resolve, reject) => {
        const offset = params.currentPage * params.pageSize
        const pageSize = params.pageSize
        connection.query('select count(*) as total from cliente', (err, result) =>{
            const total = result[0].total
            const totalPages = parseInt(total/pageSize)
            if(err){
                reject(err)
            }else{
                connection.query(`select * from cliente limit ${offset}, ${pageSize}`, (err, result) =>{
                    if(err){
                        reject(err)
                    }else{
                        resolve({
                            data: result,
                            pagination: {
                                pages: totalPages,
                                pageSize,
                                currentPage: parseInt(params.currentPage)
                            }
                        })
                    }
                })
            }
        })        
    })
}

const findbyID = (connection, id) => {
    return new Promise ((resolve, reject) => {
        connection.query('select * from cliente where codcliente = '+id, (err, result) =>{
            if(err){
                reject(err)
            }else{
                if(result.length>0){
                    resolve(result[0])
                }
                else{
                    resolve({})
                }
            }
        })
    })
}


const deletePessoa = (connection, id) => {
    return new Promise ((resolve, reject) => {
        connection.query('delete from cliente where codcliente = '+id, (err, result) => {
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    }) 
}

const createPessoas = (connection, data) => {
    return new Promise((resolve, reject) => {
        connection.query(`insert into cliente (nomecliente, endereco, cidade, cep, uf, cgc, ie) values ('${data.nome}', '${data.endereco}', '${data.cidade}', '${data.cep}', '${data.uf}', '${data.cpf}', '${data.ie}')`, (err, result) => {
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    })
}

const update = (connection, id, data) => {
    return new Promise((resolve, reject) => {
        connection.query(`update cliente set nomecliente = '${data.nome}', endereco = '${data.endereco}', cidade = '${data.cidade}', cep = '${data.cep}', uf = '${data.uf}', cgc = '${data.cpf}', ie = '${data.ie}' where codcliente = ${id}` , (err, result) => {
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    })
}

module.exports = {
    findAll,findbyID, deletePessoa, createPessoas, update
}